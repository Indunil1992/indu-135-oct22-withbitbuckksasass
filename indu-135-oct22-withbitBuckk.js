const AWS = require('aws-sdk');
const sns = new AWS.SNS();

exports.handler = async (event) => {
    try {
        let data = await sns.publish({
            Message: `kk`,
            TopicArn: `arn:aws:sns:eu-west-1:${process.env.SIGMA_AWS_ACC_ID}:induuu`,
            MessageStructure: "String",
            MessageAttributes: {}
        }).promise();

    } catch (err) {
        // error handling goes here
    };
    // add comment  
    return { "message": "Successfully executed" };
};